import { makePrismaSchema, prismaObjectType } from 'nexus-prisma';
import { PrismaSchemaConfig } from 'nexus-prisma/dist/types';
import { PrismaObjectDefinitionBlock } from 'nexus-prisma/dist/blocks/objectType';
import dataModelInfo from '../generated/nexus-prisma';
import plur from 'plur';

const connections = dataModelInfo.schema.__schema.types
  .filter(t => t.name.endsWith('Connection')).map(t => t.name);

export function fixConnections(t: PrismaObjectDefinitionBlock<'Query'>) {
  return;
  connections.forEach(c => {
    const manyC = `${plur(c.toLowerCase())}Connection`; // fix it
    t.field(manyC, {
      ...(t.prismaType as any)[manyC],
      resolve(root, args, ctx) {
        return ctx.prisma[manyC]().$fragment(`fragment Connection on ${c} {
          pageInfo {
            hasNextPage
            hasPreviousPage
            startCursor
            endCursor
          }
          aggregate { count }
          edges {
            cursor
            node { id }
          }
        }`);
      }
    });
  });
}

export function makePrismaSchema_fixed(options: PrismaSchemaConfig) {
  options.types = options.types.concat(connections.map(c => prismaObjectType({
    name: c,
    definition: (t: any) => {
      t.prismaFields(['*']);
      t.field('aggregate', {
        ...t.prismaType.aggregate,
        resolve: (root: any) => ({ count: root.edges.length }) // root.aggregate
      });
    }
  } as any)));
  return makePrismaSchema(options);
}
