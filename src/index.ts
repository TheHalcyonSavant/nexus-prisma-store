import { GraphQLServer } from 'graphql-yoga';
import { formatError } from 'apollo-errors';
import bodyParser from 'body-parser';
import { prismaObjectType } from 'nexus-prisma';
import { join } from 'path';
import { prisma } from './generated/prisma-client';
import datamodelInfo from './generated/nexus-prisma';
import { fixConnections, makePrismaSchema_fixed } from './prismaBugFixes';
import { mailer } from './third-party/nodemailer';
import resolvers from './resolvers';
import {
  createChargeAndUpdateOrder,
  updateOrder,
} from './resolvers/Mutation/stripe';
import './firebase';
import { getUser, loginField, permissions } from './resolvers/Mutation/auth';

const Stripe = require('stripe');

const Query = prismaObjectType({
  name: 'Query',
  definition: (t) => {
    fixConnections(t);
    t.prismaFields(['*']);
  }
});
const Mutation = prismaObjectType({
  name: 'Mutation',
  definition: (t) => {
    loginField(t);
    t.prismaFields(['*']);
  }
});
const schema = makePrismaSchema_fixed({
  types: [Query, Mutation],
  prisma: {
    datamodelInfo,
    client: prisma
  },
  outputs: {
    schema: join(__dirname, './generated/schema.graphql'),
    typegen: join(__dirname, './generated/nexus.ts'),
  },
});
const stripe = new Stripe(process.env.STRIPE_SECRET_KEY || '');

export const server = new GraphQLServer({
  schema,
  //resolvers: {},
  context: async req => ({
    ...req,
    prisma,
    mailer,
    stripe,
    user: await getUser(req, prisma)
  }),
  // middlewares: [permissions],
});

server.start({
  // debug: true,
  formatError,
  formatResponse: (response: any) => {
    // console.log('formatResponse', response);
    return response;
  },
  logFunction: (r) => {
    // if (r.key === 'variables' || r.key === 'query' && !/IntrospectionQuery/.test(r.data as string))
    //   console.log(r.data);
  }}, () => console.log(`Server is running on http://localhost:4000`));

const rawBodyParser = require('body-parser').raw({ type: '*/*' });

server.express.use(bodyParser.json());
server.express.post('/stripe', rawBodyParser, async (req, res, next) => {
  let data;
  // Check if webhook signing is configured.
  if (process.env.STRIPE_WEBHOOK_SECRET) {
    // Retrieve the event by verifying the signature using the raw body and secret.
    let event;
    let signature = req.headers['stripe-signature'];
    try {
      event = stripe.webhooks.constructEvent(
        req.body,
        signature,
        process.env.STRIPE_WEBHOOK_SECRET,
      );
    } catch (err) {
      console.log(`⚠️  Webhook signature verification failed.`, err.message);
      return next();
    }
    // Extract the object from the event.
    data = event.data;
  } else {
    throw new Error('Please configure STRIPE_WEBHOOK_SECRET in .env file');
  }

  const object = data.object as any;

  // Monitor `source.chargeable` events *for 3DSecure sources only*.
  if (
    object.object === 'source' &&
    object.status === 'chargeable' &&
    object.type === 'three_d_secure'
  ) {
    const source = object;
    console.log(`🔔  Webhook received! The source ${source.id} is chargeable.`);

    await createChargeAndUpdateOrder(
      {
        sourceId: source.id,
        amount: source.metadata.amount,
        email: source.metadata.email,
        orderId: source.metadata.orderId,
        userId: source.metadata.userId,
      }, prisma);
  }

  // Monitor `source.failed` events.
  if (object.object === 'source' && object.status === 'failed') {
    const source = object;
    await updateOrder(source.metadata.orderId, 'FAILED');
  }

  next();
});
