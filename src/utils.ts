import * as jwt from 'jsonwebtoken'
import { Prisma } from './generated/prisma-client'
import * as Email from 'email-templates';
export interface Context {
  prisma: Prisma
  mailer: Email
  request: any
}

export function getUserId(ctx: Context) {
  const Authorization = ctx.request.get('Authorization')
  if (Authorization) {
    const token = Authorization.replace('Bearer ', '')
    const { userId } = jwt.verify(token, process.env.APP_SECRET || '') as { userId: string }
    return userId
  }

  throw new AuthError()
}

export function getShopIdFromUserId(userId: string, ctx: Context) {
  return ctx.prisma.$graphql(`
    query ($id: ID!) {
      user(where: { id: $id }) {
        selectedShop {
          id
        }
      }
    }`, { id: userId }).then(r => r.user.selectedShop.id);
}

export function getShopId(ctx: Context) {
  const userId = getUserId(ctx);

  return getShopIdFromUserId(userId, ctx);
}

export class AuthError extends Error {
  constructor() {
    super('Not authorized')
  }
}
