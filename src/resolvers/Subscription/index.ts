import { Context } from '../../utils';

export const Subscription = {
  updatedProduct: {
    subscribe(parent: any, args: any, ctx: Context) {
      return ctx.prisma.$subscribe.product({ mutation_in: ['UPDATED'] }).node();
    },
    resolve(payload: any) {
      return payload.updatedProduct;
    }
  },
  waitFor3DSecure: {
    subscribe: (parent: any, args: any, ctx: Context) => {
      return ctx.prisma.$subscribe.order({
        mutation_in: ['UPDATED'],
        node: {
          id: args.orderId
        }
      }).node();
    },
    resolve: (payload: any) => {
      return payload.waitFor3DSecure;
    },
  }
};
