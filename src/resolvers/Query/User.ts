import { Context, getUserId, getShopIdFromUserId } from "../../utils";

export const User = {
  // Grab only orders made to the selectedShop of the user,
  // So that he cannot add an order to his cart with product
  // That were ordered from another Shop (that can potentially not be existing)
  async orders(parent: any, args: any, ctx: Context) {
    const userId = getUserId(ctx);
    const shopId = await getShopIdFromUserId(userId, ctx);
    const ordersIds = parent.orders.map((order: any) => order.id);

    return ctx.prisma.orders({
      where: {
        id_in: ordersIds,
        owner: { id: userId },
        receiver: { id: shopId }
      }
    })
  },

  async cart(parent: any, args: any, ctx: Context) {
    const userId = getUserId(ctx);
    const shopId = await getShopIdFromUserId(userId, ctx);
    const lineItemIds = parent.cart.map((lineItem: any) => lineItem.id);

    return ctx.prisma.orderLineItems({
      where: {
        id_in: lineItemIds,
        shop: { id: shopId },
        owner: { id: userId },
      }
    })
  },
}
