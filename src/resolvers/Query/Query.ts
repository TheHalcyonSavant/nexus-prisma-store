import { getUserId, getShopId, Context } from '../../utils'

export const Query = {
  me(parent: any, args: any, ctx: Context) {
    return ctx.prisma.user({ id: getUserId(ctx) });
  },
  product(parent: any, args: any, ctx: Context) {
    return ctx.prisma.product({ id: args.id });
  },
  async allProducts(parent: any, args: any, ctx: Context) {
    const shopId = await getShopId(ctx);

    return ctx.prisma.products({
      where: { deletedAt: null, shop: { id: shopId } }
    });
  },
  async searchProducts(parent: any, args: any, ctx: Context) {
    const shopId = await getShopId(ctx);

    const where = {
      ...(args.brandsIds && args.brandsIds.length > 0 && { brand: { id_in: args.brandsIds } }),
      ...(args.attributesIds && args.attributesIds.length > 0 && { attributes_some: { id_in: args.attributesIds } }),
      ...(args.optionsValuesIds && args.optionsValuesIds.length > 0 && { options_some: { values_some: { id_in: args.optionsValuesIds } } }),
      ...(!!args.productName && { name_contains: args.productName }),
      ...(!!args.categoryId && { category: { id: args.categoryId } }),
      shop: { id: shopId },
      deletedAt: null,
    };

    return ctx.prisma.productsConnection({
      where,
      first: args.first,
      skip: args.skip
    });
  },
  async allBrands(parent: any, args: any, ctx: Context) {
    const shopId = await getShopId(ctx);

    if (args.categoryId) {
      return ctx.prisma.brands({ where: { category: { id: args.categoryId }, shop: { id: shopId } } });
    }

    return ctx.prisma.brands({ where: { shop: { id: shopId } } });
  },
  async allCategories(parent: any, args: any, ctx: Context) {
    const shopId = await getShopId(ctx);

    return ctx.prisma.categories({ where: { shop: { id: shopId } } });
  },
  async allOptions(parent: any, args: any, ctx: Context) {
    const shopId = await getShopId(ctx);

    if (args.categoryId) {
      return ctx.prisma.options({ where: { category: { id: args.categoryId }, shop: { id: shopId } } });
    }

    return ctx.prisma.options({ where: { shop: { id: shopId } } });
  },
  async allAttributes(parent: any, args: any, ctx: Context) {
    const shopId = await getShopId(ctx);

    if (args.categoryId) {
      return ctx.prisma.attributes({ where: { category: { id: args.categoryId }, shop: { id: shopId } } });      
    }

    return ctx.prisma.attributes({ where: { shop: { id: shopId } } });
  },
  async allOrders(parent: any, args: any, ctx: Context) {
    const shopId = await getShopId(ctx);

    return ctx.prisma.orders({ where: { receiver: { id: shopId } } });
  },
  async allCustomers(parent: any, args: any, ctx: Context) {
    const shopId = await getShopId(ctx);
    return ctx.prisma.users({
      where: {
        orders_some: {
          receiver: { id: shopId }
        }
      }
    });
  },
  async allShops(parent: any, args: any, ctx: Context) {
    return ctx.prisma.shops({});
  }
};
