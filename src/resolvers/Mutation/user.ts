import { Context, getUserId } from '../../utils';
import { UserUpdateInput } from '../../generated/prisma-client';

export const user = {
  async updateUser(parent: any, args: any, ctx: Context) {
    const userId = getUserId(ctx);

    const data: UserUpdateInput = {
      ...(args.firstName && { firstName: args.firstName }),
      ...(args.lastName && { lastName: args.lastName }),
      ...(args.oneSignalUserId && { oneSignalUserId: args.oneSignalUserId }),
      ...(args.selectedShopId && { selectedShop: { connect: { id: args.selectedShopId } } }),
    };

    return ctx.prisma.updateUser({
      where: { id: userId },
      data,
    });
  } 
}
