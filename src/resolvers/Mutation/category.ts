import { Context, getShopId } from '../../utils';

export const category = {
  async upsertCategory(parent: any, args: any, ctx: Context) {
    const shopId = await getShopId(ctx);

    if (args.categoryId) {
      return ctx.prisma.updateCategory({
        where: { id: args.categoryId },
        data: { name: args.name }
      });
    }

    return ctx.prisma.createCategory({
      name: args.name,
      shop: { connect: { id: shopId } }
    });
  },

  deleteCategory(parent: any, args: any, ctx: Context) {
    return ctx.prisma.deleteCategory({ id: args.categoryId });
  },
};
