import { Context, getShopId } from "../../utils";

export const brand = {
  async upsertBrand(parent: any, args: any, ctx: Context) {
    if (args.brandId) {
      return ctx.prisma.updateBrand({
        where: { id: args.brandId },
        data: { name: args.name, category: { connect: { id: args.categoryId } } }
      });
    }

    const shopId = await getShopId(ctx);

    return ctx.prisma.createBrand({
      name: args.name,
      shop: { connect: { id: shopId } },
      category: { connect: { id: args.categoryId } },
    });
  },

  deleteBrand(parent: any, args: any, ctx: Context) {
    return ctx.prisma.deleteBrand({ id: args.brandId });
  },
}
