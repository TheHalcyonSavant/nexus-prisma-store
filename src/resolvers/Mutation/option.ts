import { Context, getShopId } from '../../utils';
import * as _ from 'lodash';

export const option = {
  async upsertOption(parent: any, args: any, ctx: Context) {
    if (args.optionId) {
      const currentOption = await ctx.prisma.$graphql(`
        query ($id: ID!) {
          option (where: { id: $id }) {
            values {
              id
              name
            }
          }
        }`, { id: args.optionId }).then((r: any) => r.option);

    const optionValuesToDelete = _.differenceBy(currentOption.values, args.values, 'id');
    const optionValuesToDeleteInput = optionValuesToDelete.map((optionValue: any) => ({ id: optionValue.id }))

    const optionValuesToCreate = _(args.values)
      .differenceBy(currentOption.values, 'id')
      .map((optionValue: any) => ({ name: optionValue.value }))
      .value();

    const optionValuesToUpdate = args.values
    .filter((optionValue: any) =>
      !!currentOption.values.find((currentOptionValue: any) => currentOptionValue.id === optionValue.id)
    )
    .map((optionValue: any) => ({
      where: { id: optionValue.id },
      data: { name: optionValue.value }
    }));

      // Some option values were deleted
      if (args.values.length < currentOption.values.length) {
        // Find all variants that had one of the removed option value
        const variantsToDelete = await Promise.all(
          optionValuesToDelete.map((optionValueToDelete: any) => (
            ctx.prisma.variants({
              where: { selectedOptions_some: {
                option: { id: args.optionId },
                value: { id: optionValueToDelete.id }
              }}
            })
          ))
        );
        
        // Remove all the variants and associated selectedOptions
        // /!\ Cascading delete not working with deleteManyType
        await Promise.all(
          optionValuesToDelete.map((optionValueToDelete: any) => (
            ctx.prisma.deleteManySelectedOptions({
              option: { id: args.optionId },
              value: { id: optionValueToDelete.id }
            })
          ))
        )

        await Promise.all(
          variantsToDelete.map((variantsForOptionValue) => (
            ctx.prisma.deleteManyVariants({ id_in: variantsForOptionValue.map((variant: any) => variant.id) })
          ))
        )
      }

      return ctx.prisma.updateOption({
        where: { id: args.optionId },
        data: {
          name: args.name,
          category: { connect: { id: args.categoryId } },
          values: {
            update: optionValuesToUpdate,
            create: optionValuesToCreate,
            delete: optionValuesToDeleteInput
          }
        }
      });
    }

    const shopId = await getShopId(ctx);
    const createValuesInput = args.values.map((optionValue: any) => ({ name: optionValue.value }));

    return ctx.prisma.createOption({
      name: args.name,
      shop: { connect: { id: shopId } },
      values: { create: createValuesInput },
      category: { connect: { id: args.categoryId } }
    });
  },

  deleteOption(parent: any, args: any, ctx: Context) {
    return ctx.prisma.deleteOption({ id: args.optionId });
  },
};
