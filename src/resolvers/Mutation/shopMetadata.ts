import { Context, getShopId } from '../../utils';
import * as _ from 'lodash';

interface OrderableProductInput {
  id: string,
  productId: string,
  position: number
}

export const shopMetadata = {
  async upsertBestSellerProducts(parent: any, args: any, ctx: Context) {
    const shopId = await getShopId(ctx);

    const buildCreateInput = (products: [any]) =>
      products.map((bestSellerProduct: OrderableProductInput) => ({
        product: { connect: { id: bestSellerProduct.productId } },
        position: bestSellerProduct.position
      }));

    const currentShop = await ctx.prisma.$graphql(`
      query ($id: ID!) {
        shop (where: { id: $id }) {
          bestSellerProducts {
            id
            product { id }
          }
        }
      }`, { id: shopId }).then((r: any) => r.shop);

    const productsToDelete = _(currentShop.bestSellerProducts)
      .differenceBy(args.bestSellerProducts, 'id')
      .map((orderableProduct: any) => ({ id: orderableProduct.id }))
      .value();
    const productsToConnect = _.differenceBy(args.bestSellerProducts, currentShop.bestSellerProducts, 'id')
    const productsToUpdate = args.bestSellerProducts
      .filter((orderableProduct: OrderableProductInput) =>
        !!currentShop.bestSellerProducts.find((currentProduct: any) => currentProduct.id === orderableProduct.id)
      )
      .map((orderableProduct: OrderableProductInput) => ({
        where: { id: orderableProduct.id },
        data: { position: orderableProduct.position }
      }));

    return ctx.prisma.updateShop({
      where: { id: shopId },
      data: {
        bestSellerProducts: {
          create: buildCreateInput(productsToConnect),
          delete: productsToDelete,
          update: productsToUpdate,
        }
      },
    });
  },
  
  async upsertNewProducts(parent: any, args: any, ctx: Context) {
    const shopId = await getShopId(ctx);

    const buildCreateInput = (products: any) =>
      products.map((newProduct: OrderableProductInput) => ({
        product: { connect: { id: newProduct.productId } },
        position: newProduct.position
      }));

    const currentShop = await ctx.prisma.$graphql(`
      query ($id: ID!) {
        shop (where: { id: $id }) {
          newProducts {
            id
            product { id }
          }
        }
      }`, { id: shopId });

    const productsToDelete = _(currentShop.newProducts)
      .differenceBy(args.newProducts, 'id')
      .map((orderableProduct: any) => ({ id: orderableProduct.id }))
      .value();
    const productsToConnect = _.differenceBy(args.newProducts, currentShop.newProducts, 'id')
    const productsToUpdate = args.newProducts
      .filter((orderableProduct: OrderableProductInput) =>
        !!currentShop.newProducts.find((currentProduct: any) => currentProduct.id === orderableProduct.id)
      )
      .map((orderableProduct: OrderableProductInput) => ({
        where: { id: orderableProduct.id },
        data: { position: orderableProduct.position }
      }));

    return ctx.prisma.updateShop({
      where: { id: shopId },
      data: {
        newProducts: {
          create: buildCreateInput(productsToConnect),
          delete: productsToDelete,
          update: productsToUpdate,
        }
      },
    });
  },

  async updateMOTD(parent: any, args: any, ctx: Context) {
    const shopId = await getShopId(ctx);

    return ctx.prisma.updateShop({
      where: { id: shopId },
      data: { MOTD: args.MOTD }
    });
  },
}
