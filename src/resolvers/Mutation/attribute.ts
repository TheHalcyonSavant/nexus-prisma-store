import { Context, getShopId } from '../../utils';

export const attribute = {
  async upsertAttribute(parent, args, ctx: Context, info) {
    if (args.attributeId) {
      return ctx.prisma.updateAttribute({
        where: { id: args.attributeId },
        data: { value: args.value, category: { connect: { id: args.categoryId } } }
      });
    }

    const shopId = await getShopId(ctx);

    return ctx.prisma.createAttribute({
      value: args.value,
      shop: { connect: { id: shopId } },
      category: { connect: { id: args.categoryId } }
    });
  },

  deleteAttribute(parent, args, ctx: Context, info) {
    return ctx.prisma.deleteAttribute({ id: args.attributeId });
  },
};
