import { Context, getUserId, getShopId, getShopIdFromUserId } from "../../utils";
import * as _ from 'lodash';
import { OrderLineItem } from "../../generated/prisma-client";
import {
  OrderLineItemNotFoundException,
  OrderNotFoundException,
  ProductNotFoundException,
  ProductOrVariantNotFoundException,
  OrderNotSentToCurrentShopException,
} from '../../exceptions';
import { createError } from "apollo-errors";

interface OrderLineItemInput {
  variantId: string,
  quantity: number
}

export const cart = {
  //args: VariantId: ID!, quantity: Int!
  async addItemToCart(parent: any, args: any, ctx: Context) {
    const userId = getUserId(ctx);
    const shopId = await getShopIdFromUserId(userId, ctx);

    if (!(await ctx.prisma.$exists.variant({ id: args.variantId }))) {
      throw new ProductNotFoundException();
    }

    // Find if there's any existing lineItem. If so, update the quantity
    const orderLineItems = await ctx.prisma.orderLineItems({
      where: {
        owner: { id: userId },
        variant: { id: args.variantId },
      }
    });

    if (orderLineItems.length > 1) {
      throw new Error('More than one same line item found in the cart. Should not be possible');
    }

    if (orderLineItems.length === 1) {
      const orderLineItem = orderLineItems[0];

      return ctx.prisma.updateOrderLineItem({
        where: { id: orderLineItem.id },
        data: {
          quantity: args.mergeQuantities ? args.quantity + orderLineItem.quantity : args.quantity
        }
      });
    }

    return ctx.prisma.createOrderLineItem({
      owner: { connect: { id: userId } },
      shop: { connect: { id: shopId } },
      quantity: args.quantity,
      variant: { connect: { id: args.variantId } }
    });
  },

  // args orderId: ID!, replace: Boolean!
  async addOrderToCart(parent: any, args: any, ctx: Context): Promise<OrderLineItem[]> {
    const userId = getUserId(ctx);
    const shopId = await getShopId(ctx);

    const order = await ctx.prisma.$graphql(`
      query ($id: ID!) {
        order(where: { id: $id }) {
          id
          receiver {
            id
          }
          lineItems {
            id
            quantity
            variant {
              id
              product {
                id
              }
            }
          }
        }
      }
    `, { id: args.orderId }).then((r: any) => r.order);


    if (order.receiver.id !== shopId) {
      throw new OrderNotSentToCurrentShopException();
    }

    if (!order) {
      throw new OrderNotFoundException();
    }

    // Add to the cart only the lineItems that have a product that still exists
    const existingProducts = await Promise.all(
      order.lineItems.map((lineItem: any) => ctx.prisma.$exists.product({ id: lineItem.variant.product.id, deletedAt: null }))
    );
    const lineItemsWithExistingProduct = order.lineItems.filter((_: any, i: number) => existingProducts[i]);

    const currentCart = await ctx.prisma.$graphql(`
      query ($id: ID!) {
        user(where: { id: $id }) {
          cart {
            id
            quantity
            variant {
              id
            }
          }
        }
      }`, { id: userId }).then((r: any) => r.user.cart);

    // Delete all items from cart that are not in the order
    if (args.replace) {
      const cartLineItemsToDelete = _.differenceBy(currentCart, lineItemsWithExistingProduct, (lineItem: any) => lineItem.variant.id);

      await Promise.all(
        cartLineItemsToDelete.map(lineItem => ctx.prisma.deleteOrderLineItem({ id: lineItem.id }))
      );
    }

    return await Promise.all(
      lineItemsWithExistingProduct.map((orderLineItem: any) => {
        const cartOrderLineItem = currentCart.find((cartLineItem: any) => cartLineItem.variant.id === orderLineItem.variant.id);

        if (cartOrderLineItem) {
          return ctx.prisma.updateOrderLineItem({
            where: { id: cartOrderLineItem.id },
            data: {
              quantity: args.replace
                ? orderLineItem.quantity
                : orderLineItem.quantity + cartOrderLineItem.quantity
            }
          });
        }

        return ctx.prisma.createOrderLineItem({
          owner: { connect: { id: userId } },
          shop: { connect: { id: shopId } },
          quantity: orderLineItem.quantity,
          variant: { connect: { id: orderLineItem.variant.id } }
        });
      })
    )
  },

  async removeItemFromCart(parent: any, args: any, ctx: Context): Promise<OrderLineItem> {
    const userId = getUserId(ctx);

    if (!(await ctx.prisma.$exists.user({ id: userId, cart_some: { id: args.lineItemId } }))) {
      throw new Error('You\'re not owner of this cart.');
    }

    if (!(await ctx.prisma.$exists.orderLineItem({ id: args.lineItemId }))) {
      throw new OrderLineItemNotFoundException();
    }

    return ctx.prisma.deleteOrderLineItem({
      id: args.lineItemId
    });
  },

  async updateItemFromCart(parent: any, args: any, ctx: Context): Promise<OrderLineItem> {
    const userId = getUserId(ctx);

    if (!(await ctx.prisma.$exists.user({ id: userId, cart_some: { id: args.lineItemId } }))) {
      throw new Error('You\'re not owner of this cart.');
    }

    if (await !ctx.prisma.$exists.orderLineItem({ id: args.lineItemId })) {
      throw new Error('This line item doesn\'t exist anymore');
    }

    return ctx.prisma.updateOrderLineItem({
      where: { id: args.lineItemId },
      data: {
        variant: {
          connect: { id: args.variantId },
        },
        quantity: args.quantity
      }
    });
  },
};
