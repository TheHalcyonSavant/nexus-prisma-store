import { Context, getShopId } from '../../utils';
import * as _ from 'lodash';
import { ID_Input } from '../../generated/prisma-client';

export const product = {
  async upsertProduct(parent: any, args: any, ctx: Context) {
    const optionsToConnect = args.optionIds.map((optionId: string) => ({ id: optionId }));
    const attributesToConnect = args.attributesIds.map((attributeId: string) => ({ id: attributeId }));
    const selectedOptionsForVariantInput = (variant: any) => variant.selectedOptions.map((selectedOption: any) => ({
      option: { connect: { id: selectedOption.optionId } },
      value: { connect: { id: selectedOption.valueId } }
    }));
    const createVariantsInput = (variants: [any]) => variants.map((variant) => ({
      price: variant.price,
      available: variant.available,
      selectedOptions: { create: selectedOptionsForVariantInput(variant) }
    }));
    const shopId = await getShopId(ctx);

    if (args.productId) {
      const currentProduct = await ctx.prisma.$graphql(`
        query ($id: ID!) {
          product (where: { id: $id }) {
            id
            attributes { id }
            options { id }
            variants { id }
            unavailableOptionsValues { id }
          }
        }`, { id: args.productId }).then((r: any) => r.product);

      // 1. If some variants were removed (eg: an option value were unselected from the product)
      // 2. Soft-delete the variants, their associated selectedOptions, and the orderLineItems that had those variants
      // Remark: Ideally, we should only soft-delete the variants that are in orders, and hard-delete the others, but we make it easier this way
      // Remark: We don't disconnect the variants from the product so that users can still see the items that were deleted from their cart.
      // Remark: Because of that, we need to filter the soft_deleted variants everywhere else. :(
      const variantsToDelete = _.differenceBy(currentProduct.variants, args.variants, 'id');

      if (variantsToDelete.length > 0) {
        const variantsIdsToDelete = variantsToDelete.map((variant: any) => variant.id);
        const deletedAt = new Date().toISOString();
      
        await ctx.prisma.updateManySelectedOptions({
          where: {
            variant: {
              id_in: variantsIdsToDelete,
              product: { id: args.productId } }
          },
          data: { deletedAt }
        });

        await ctx.prisma.updateManyVariants({
          where: {
            id_in: variantsIdsToDelete,
            product: { id: args.productId }
          },
          data: { deletedAt }
        });

        await ctx.prisma.updateManyOrderLineItems({
          where: {
            variant: { id_in: variantsIdsToDelete }
          },
          data: { deletedAt }
        });
      }

      const attributesToDisconnect = _(currentProduct.attributes.map(({ id }) => id ))
        .difference(args.attributesIds)
        .map((attributeId: ID_Input) => ({ id: attributeId }))
        .value();

      const optionsToDisconnect = _(currentProduct.options.map(({ id }) => id ))
        .difference(args.optionIds)
        .map((optionId: ID_Input) => ({ id: optionId }))
        .value();

      const currentUnavailableOptionsValuesIds = currentProduct.unavailableOptionsValues.map(optionValue => optionValue.id);
      const unavailableOptionsValuesToConnect = _(args.unavailableOptionsValuesIds)
        .differenceBy(currentUnavailableOptionsValuesIds)
        .map((optionValueId: ID_Input) => ({ id: optionValueId }))
        .value();
      const unavailableOptionsValuesToDisconnect = _(currentUnavailableOptionsValuesIds)
        .differenceBy(args.unavailableOptionsValuesIds)
        .map((optionValueId: ID_Input) => ({ id: optionValueId }))
        .value();
      
      const variantsToCreate = _.differenceBy(args.variants, currentProduct.variants, 'id');
      const variantsToUpdate = args.variants
      .filter((variant: any /* ProductVariantInput */) =>
        !!currentProduct.variants.find((currentVariant: any) => currentVariant.id === variant.id)
      )
      .map((variant: any) => ({
        where: { id: variant.id },
        data: {
          available: variant.available,
          price: variant.price
        }
      }));

      return ctx.prisma.updateProduct({
        where: { id: args.productId },
        data: {
          name: args.name,
          description: args.description,
          available: args.available,
          category: { connect: { id: args.categoryId } },
          brand: { connect: { id: args.brandId } },
          attributes: {
            connect: attributesToConnect,
            disconnect: attributesToDisconnect
          },
          options: {
            connect: optionsToConnect,
            disconnect: optionsToDisconnect
          },
          unavailableOptionsValues: {
            disconnect: unavailableOptionsValuesToDisconnect,
            connect: unavailableOptionsValuesToConnect,
          },
          displayPrice: args.displayPrice,
          SKU: "",
          variants: {
            update: variantsToUpdate,
            create: createVariantsInput(variantsToCreate),
          },
          imageUrl: args.imageUrl
        }
      });
    }
    
    return ctx.prisma.createProduct({
      name: args.name,
      description: args.description,
      available: args.available,
      shop: { connect: { id: shopId } },
      category: { connect: { id: args.categoryId } },
      brand: { connect: { id: args.brandId } },
      options: { connect: optionsToConnect },
      displayPrice: args.displayPrice,
      SKU: "",
      attributes: { connect: attributesToConnect },
      variants: { create: createVariantsInput(args.variants) },
      imageUrl: args.imageUrl
    });
  },

  async deleteProduct(parent: any, args: any, ctx: Context) {
    const shopId = await getShopId(ctx);
    // If product is in some user's cart
    // Disconnect it from the carts
    const usersWithProductInCart = await ctx.prisma.users({
      where: {
        selectedShop: { id: shopId },
        cart_some: {
          variant: {
            product: { id: args.productId }
          }
        }
      }
    });

    if (usersWithProductInCart.length > 0) {
      const usersToDisconnectIds = usersWithProductInCart.map((user: any) => user.id);

      await ctx.prisma.updateManyOrderLineItems({
        where: {
          owner: {
            id_in: usersToDisconnectIds,
            selectedShop: { id: shopId },
          },
          variant: {
            product: { id: args.productId }
          }
        },
        data: { deletedAt: new Date().toISOString() },
      });
    }

    const productIsInOrderOrCart = await ctx.prisma.$exists.orderLineItem({
      variant: {
        product: { id: args.productId }
      }
    });

    // If product is in some orders or user's cart, then soft-delete the product
    if (productIsInOrderOrCart) {
      //Still delete the new/best-sellers products.
      await ctx.prisma.deleteManyOrderableProducts({ product: { id: args.productId } });

      return ctx.prisma.updateProduct({
        where: { id: args.productId },
        data: { deletedAt: new Date().toISOString() }
      });
    }

    return ctx.prisma.deleteProduct({ id: args.productId });
  },
}
