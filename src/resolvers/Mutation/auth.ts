import * as bcrypt from 'bcryptjs';
import * as admin from 'firebase-admin';
import { resolve } from 'dns';
import { generate } from 'generate-password';
import { rule, shield, and, allow, deny } from 'graphql-shield';
import { ContextParameters } from 'graphql-yoga/dist/types';
import * as jwt from 'jsonwebtoken';
import { stringArg } from 'nexus';
import * as validator from 'validator';
import { Context, getUserId } from '../../utils';
import { User, Role, Prisma } from '../../generated/prisma-client';
import {
  InvalidEmailException,
  InvalidOldPasswordException,
  InvalidPasswordException,
} from '../../exceptions';
import { user } from './user';

const fbAuth = admin.auth();
const isAuthenticated = rule()(async (parent, args, ctx, info) => !!ctx.user);
const isAdmin = rule()(async (parent, args, ctx, info) => {
  console.log('isAdmin');
  console.log(ctx.user);
  return ctx.user.role === 'ADMIN'
});

// Deni THS token:
const httpHeadersForTesting =
{
  "Authorization": "Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6IjY1NmMzZGQyMWQwZmVmODgyZTA5ZTBkODY5MWNhNWM3ZjJiMGQ2MjEiLCJ0eXAiOiJKV1QifQ.eyJuYW1lIjoiRGVuaSBUSFMiLCJwaWN0dXJlIjoiaHR0cHM6Ly9saDQuZ29vZ2xldXNlcmNvbnRlbnQuY29tLy1oeEpacjhVaWdCTS9BQUFBQUFBQUFBSS9BQUFBQUFBQUFBQS9BQ0hpM3JmdVc4ZUpVY1BvU29Tc29LQ09kX2s2QU9XT29BL21vL3Bob3RvLmpwZyIsImlzcyI6Imh0dHBzOi8vc2VjdXJldG9rZW4uZ29vZ2xlLmNvbS9oYXplbC1jZWRhci01NDAiLCJhdWQiOiJoYXplbC1jZWRhci01NDAiLCJhdXRoX3RpbWUiOjE1NTU5ODkyMjcsInVzZXJfaWQiOiIwMmVRTW9OQ1l4VFQ3VGtzU2JCWVpMZWFuamgyIiwic3ViIjoiMDJlUU1vTkNZeFRUN1Rrc1NiQllaTGVhbmpoMiIsImlhdCI6MTU1NTk4OTIyNywiZXhwIjoxNTU1OTkyODI3LCJlbWFpbCI6InRoZWhhbGN5b25zYXZhbnRAZ21haWwuY29tIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImZpcmViYXNlIjp7ImlkZW50aXRpZXMiOnsiZ29vZ2xlLmNvbSI6WyIxMDM0MjQzNTc4NzU1MjQzNjI0NTMiXSwiZW1haWwiOlsidGhlaGFsY3lvbnNhdmFudEBnbWFpbC5jb20iXX0sInNpZ25faW5fcHJvdmlkZXIiOiJnb29nbGUuY29tIn19.ToQ0eWlmX9JAJ0288iuk_PXoEoYB19yf6ZrHEYfeihp7nZmHRqC3_W78j5bD1vnl-8dA33s5Bk74lmrU8wiE4l6mpyrsuRsJfrCbK3IXtEqaytHVF9gRTHkzpESc5nI4SqvmzmBNPFX1VkDQDXfAuwA06-2v0la2VS6CNsug9-kA1givrj3iIcfC65Cdys6_u7YbxKnxr_a3lA7_4sEpzB0dHmXi_Rj2wVYUoOHzuiXmKwyc3ZQ04tCEPj6O-dWIPlahUkLOwdrFF5TYh4rGsjNSi64zM5NdLIs2QRXvG2KwHvebuIdqn0HtW2shVOqpwP2HKCCAkU9q0ADSHb-kGw"
}
export async function getUser(cp: ContextParameters, prisma: Prisma) {
  const Authorization = cp.request.get('Authorization');
  let user = null;
  if (!Authorization) return user;
  try {
    const token = Authorization.replace('Bearer ', '');
    let { provider_id, email } = await fbAuth.verifyIdToken(token);
    if (provider_id === 'anonymous') email = 'err8086@gmail.com';
    user = await prisma.user({ email });
  } catch (ex) {
    console.log(ex.message);
    return null;
  }
  return user;
}

export function loginField(t: any) {
  t.field('login', {
    // nullable: true,
    args: {
      token: stringArg(),
      selectedShop: stringArg(),
    },
    type: 'User',
    resolve: async (parent: any, { token, selectedShop }: any, ctx: Context) => {
      const claim = await fbAuth.verifyIdToken(token);
      const { provider_id, email, uid: firebaseId, name: displayName } = claim;
      if (provider_id === 'anonymous') return ctx.prisma.user({ email: 'err8086@gmail.com' });

      const data = {
        email,
        firebaseId,
        selectedShop: {
          connect: {
            id: selectedShop
          }
        },
        displayName,
        role: 'ADMIN' as Role
      };
      return ctx.prisma.upsertUser({
        where: { email },
        create: data,
        update: data
      });
    },
  });
}

export const permissions = shield({
  '__Schema': {
      '*': deny,
  },
  // Query: {
  //   '*': isAuthenticated,
  // },
  // Mutation: {
  //   '*': and(isAuthenticated, isAdmin),
  //   login: allow,
  // },
}, {
  fallbackRule: deny,
  allowExternalErrors: process.env.NODE_ENV !== 'production'
});

// function hashPassword(password: string) {
//   return bcrypt.hash(password, 10);
// }

export const auth = {
  // async signup(parent: any, args: any, ctx: Context) {
  //   const password = await hashPassword(args.password);
  //   const user = await ctx.prisma.createUser({
  //     email: args.email,
  //     password,
  //     firstName: args.firstName,
  //     lastName: args.lastName,
  //     selectedShop: { connect: { id: args.shopId } },
  //     role: 'USER',
  //   });

  //   return {
  //     token: jwt.sign({ userId: user.id }, process.env.APP_SECRET || ''),
  //     user,
  //   };
  // },

  // async login(parent: any, { email, password }: any, ctx: Context) {
  //   const user = await ctx.prisma.user({ email: email.toLowerCase() });
  //   if (!user) {
  //     throw new Error(`No such user found for email: ${email}`);
  //   }

  //   const valid = await bcrypt.compare(password, user.password);
  //   if (!valid) {
  //     throw new Error('Invalid password');
  //   }

  //   return {
  //     token: jwt.sign({ userId: user.id }, process.env.APP_SECRET || ''),
  //     user,
  //   };
  // },

  // async resetPassword(parent: any, { email }: User, ctx: Context) {
  //   if (!validator.isEmail(email)) {
  //     throw new InvalidEmailException();
  //   }

  //   const user = await ctx.prisma.user({ email });

  //   if (!user) {
  //     return { mailMaybeSent: true };
  //   }

  //   const clearPassword = generate({
  //     length: 10,
  //     numbers: true,
  //     uppercase: true,
  //   });

  //   const hashedPassword = await hashPassword(clearPassword);

  //   await ctx.prisma.updateUser({
  //     where: { id: user.id },
  //     data: {
  //       password: hashedPassword,
  //     },
  //   });

  //   ctx.mailer.send({
  //     template: 'passwordReset',
  //     message: {
  //       to: user.email,
  //     },
  //     locals: { newPassword: clearPassword },
  //   });

  //   return {
  //     mailMaybeSent: true,
  //   };
  // },

  // async deleteAccount(parent: any, args: any, ctx: Context) {
  //   const { password } = args;

  //   const userId = getUserId(ctx);
  //   const user = await ctx.prisma.user({ id: userId });

  //   const valid = await bcrypt.compare(password, user.password);

  //   if (!valid) {
  //     throw new InvalidPasswordException();
  //   }

  //   await ctx.prisma.deleteUser({ id: userId });

  //   return {
  //     id: user!.id,
  //   };
  // },

  // async changePassword(parent: any, args: any, ctx: Context) {
  //   const { oldPassword, newPassword } = args;

  //   const userId = getUserId(ctx);
  //   const user = await ctx.prisma.user({ id: userId });

  //   const valid = await bcrypt.compare(oldPassword, user.password);

  //   if (!valid) {
  //     throw new InvalidOldPasswordException();
  //   }

  //   const password = await hashPassword(newPassword);

  //   const newUser = await ctx.prisma.updateUser({
  //     where: { id: userId },
  //     data: { password },
  //   });

  //   return {
  //     id: newUser!.id,
  //   };
  // },
};
