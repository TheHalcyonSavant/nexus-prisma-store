import { Context } from "../../utils";
import * as _ from "lodash";
import { Order, Prisma, OrderStatus } from "../../generated/prisma-client";
import { sendNotificationToOne } from "../../third-party/oneSignal";

interface CreateOrderInput {
  userId: string
  orderStatus: OrderStatus
  emptyCart: boolean
}

export async function createOrderFromCart(args: CreateOrderInput, prisma: Prisma): Promise<Order | null> {
  const { selectedShop: { id: shopId }, cart } = await prisma.$graphql(`
    query ($id: ID!) {
      user (where: { id: $id }) {
        selectedShop {
          id
        }
        cart {
          id
          quantity
          variant {
            id
            price
          }
        }
      }
    }`, { id: args.userId }).then((r: any) => r.user);

  if (cart.length === 0) {
    return null;
  }

  const lineItemsIds = cart.map((lineItem: any) => ({ id: lineItem.id }));
  const totalPrice = _.sumBy(
    cart,
    (lineItem: any) => lineItem.quantity * lineItem.variant.price
  );

  if (args.emptyCart) {
    await prisma.updateUser({
      where: { id: args.userId },
      data: {
        cart: { disconnect: lineItemsIds }
      }
    });
  }

  const newOrder = await prisma.createOrder({
    owner: { connect: { id: args.userId } },
    receiver: { connect: { id: shopId } },
    lineItems: { connect: lineItemsIds },
    totalPrice,
    totalTax: 0,
    totalRefunded: 0,
    orderStatus: args.orderStatus
  });

  return newOrder;
}

export async function emptyCartForUser(userId: string, prisma: Prisma): Promise<undefined> {
  const { cart } = await prisma.$graphql(`
    query ($id: ID!) {
      user (where: { id: $id }) {
        cart {
          id
          quantity
          variant {
            id
            price
          }
        }
      }
    }`, { id: userId }).then((r: any) => r.user);

  if (cart.length === 0) {
    return;
  }

  const lineItemsIds = cart.map((lineItem: any) => ({ id: lineItem.id }));
  const totalPrice = _.sumBy(
    cart,
    (lineItem: any) => lineItem.quantity * lineItem.variant.price
  );

  await prisma.updateUser({
    where: { id: userId },
    data: {
      cart: { disconnect: lineItemsIds }
    }
  });
}

export const order = {
  async setOrderAsPrepared(parent: any, args: any, ctx: Context) {
    const currentOrder = await ctx.prisma.$graphql(`
      query ($id: ID!) {
        order (where: { id: $id }) {
          id
          orderStatus
          owner {
            id
            oneSignalUserId
          }
        }
      }`, { id: args.orderId }).then((r: any) => r.order);

    if (currentOrder.orderStatus === 'SUBMITTED' || currentOrder.orderStatus === 'FAILED') {
      throw new Error('You can set an order to prepared, only once it has been paid.');
    }

    if (currentOrder.orderStatus === 'PREPARED') {
      throw new Error('You can set an order to prepared only once');
    }

    const updatedOrder = await ctx.prisma.updateOrder({
      where: { id: args.orderId },
      data: { orderStatus: 'PREPARED' }
    });

    if (updatedOrder) {
      // Send notification to owner of order when set to prepared
      if (currentOrder.owner.oneSignalUserId) {
        try {
          await sendNotificationToOne(
            await ctx.prisma.user({ id: updatedOrder.id }).oneSignalUserId(),
            "Your order is ready !",
            "Visit your store to pick up your order."
          );
        } catch (e) {
          console.log(e);
        }
      }
    }

    return updatedOrder;
  }
}
