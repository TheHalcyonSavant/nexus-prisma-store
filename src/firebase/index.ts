// cSpell:disable
import * as admin from 'firebase-admin';
import serviceAccount from './hazel-cedar-540-firebase-adminsdk-shfs1-4285e6e7dc.json';

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount as any),
  databaseURL: "https://hazel-cedar-540.firebaseio.com"
});

// admin.auth().verifyIdToken(idToken)
//   .then(function(decodedToken) {
//     var uid = decodedToken.uid;
//     // ...
//   }).catch(function(error) {
//     // Handle error
//   });

// https://firebase.google.com/docs/auth/admin/manage-sessions#advanced_security_enforce_ip_address_restrictions
//
// app.post('/getRestrictedData', (req, res) => {
//   // Get the ID token passed.
//   const idToken = req.body.idToken;
//   // Verify the ID token, check if revoked and decode its payload.
//   admin.auth().verifyIdToken(idToken, true).then((claims) => {
//     // Get the user's previous IP addresses, previously saved.
//     return getPreviousUserIpAddresses(claims.sub);
//   }).then(previousIpAddresses => {
//     // Get the request IP address.
//     const requestIpAddress = req.connection.remoteAddress;
//     // Check if the request IP address origin is suspicious relative to previous
//     // IP addresses. The current request timestamp and the auth_time of the ID
//     // token can provide additional signals of abuse especially if the IP address
//     // suddenly changed. If there was a sudden location change in a
//     // short period of time, then it will give stronger signals of possible abuse.
//     if (!isValidIpAddress(previousIpAddresses, requestIpAddress)) {
//       // Invalid IP address, take action quickly and revoke all user's refresh tokens.
//       revokeUserTokens(claims.uid).then(() => {
//         res.status(401).send({error: 'Unauthorized access. Please login again!'});
//       }, error => {
//         res.status(401).send({error: 'Unauthorized access. Please login again!'});
//       });
//     } else {
//       // Access is valid. Try to return data.
//       getData(claims).then(data => {
//         res.end(JSON.stringify(data);
//       }, error => {
//         res.status(500).send({ error: 'Server error!' })
//       });
//     }
//   });
// });
