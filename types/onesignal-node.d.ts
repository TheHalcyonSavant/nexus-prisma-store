export class Client {
  constructor(credentials: any);
  API_URI: any;
  addDevice(body: any, callback: any): any;
  cancelNotification(notificationId: any, callback: any): any;
  createApp(body: any, callback: any): any;
  csvExport(body: any, callback: any): any;
  editDevice(deviceId: any, body: any, callback: any): any;
  incrementSessionLength(playerId: any, body: any, callback: any): any;
  newPurchase(playerId: any, body: any, callback: any): any;
  newSession(playerId: any, body: any, callback: any): any;
  sendNotification(notification: any, callback: any): any;
  setApp(app: any): void;
  setRootUrl(rootUrl: any): void;
  trackOpen(notificationId: any, body: any, callback: any): any;
  updateApp(body: any, callback: any): any;
  viewApp(appId: any, callback: any): any;
  viewApps(callback: any): any;
  viewDevice(deviceId: any, callback: any): any;
  viewDevices(query: any, callback: any): any;
  viewNotification(notificationId: any, callback: any): any;
  viewNotifications(query: any, callback: any): any;
}
export class Notification {
  constructor(initialBody: any);
  postBody: any;
  setContent(contents: any): void;
  setExcludedSegments(excluded_segments: any): void;
  setFilters(filters: any): void;
  setIncludedSegments(included_segments: any): void;
  setParameter(name: any, value: any): void;
  setTargetDevices(include_player_ids: any): void;
}
